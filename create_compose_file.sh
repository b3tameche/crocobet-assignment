cat > ./docker-compose.yaml <<EOL
version: '2.4'

services:
  app:
    image: b3tameche/croco
    ports:
      - "8088:8088"
EOL