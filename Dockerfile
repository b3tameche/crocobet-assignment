FROM python:3.11-alpine3.17

WORKDIR /src

RUN pip install --upgrade pip
COPY ./requirements.txt /src/requirements.txt
RUN pip install -r /src/requirements.txt

COPY . /src

EXPOSE 8088

CMD [ "python3", "croco.py" ]